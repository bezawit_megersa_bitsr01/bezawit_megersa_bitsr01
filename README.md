# portfolio project
[Check out My Portfolio 🌐  ]
(https://66114b31dfac42e403cf4fc0--velvety-pothos-7ff5ed.netlify.app/)

This is a portfolio project showcasing my skills, Education status, and experiences.

## Description

This portfolio project serves as a comprehensive overview of my skills, and experiences in the field. Additionaly it describe my academic status that, I have spent on my entire life like from primary school to the university level and my performances. It also discuss my skill i have learn on different websites.

![Portfolio Project Image](./bezawit.png)

## Deployment

The project is deployed and accessible online. You can view it [here](https://66114b31dfac42e403cf4fc0--velvety-pothos-7ff5ed.netlify.app/).

Feel free to explore and reach out for any inquiries or collaborations.